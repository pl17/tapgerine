# Tapgerine Test Specification #

Application for logging a follows a tracking link.

Application have home page with displaying all logs, what can be filter/sort quick at frontend. 

Also it contain set-up panel for edit `Bad domains` settings what help
processing the following by links.

Also it contain Demo page with generating example of tracking links.

More information about task at `docs/test_case_for_back-end_developer.pdf`

## Requirements ##

* PHP 7.0
* Internet connection
* npm
* composer

## Technologies ##

Project based on frameworks Laravel 5.4
At home page used AngularJS.

### Installation ###

* Put that project to your place

```bash
composer install
npm install
```

* Set-up your configuration at file `.env` (at least DB-settings).

```bash
php artisan key:generate
php artisan migrate
```

Then set-up your server.
Or can just use internal tools for run server at `http://127.0.0.1:8000` by
following command:

```bash
php artisan serve
```

## Why I choose that technologies? ##

* Laravel - I can be use that framework at other place.
And also I must use other framework at yours company what I did not know.

So I decided to learn something new and demonstrate how I can use new knowledge.

* AngularJS - Same reason. I had been seen that technology at my current office.
But never try old version. Before I develop something little only at Angular.

***
Be lenient with the assessment. :-) It my first experience with that two technology.
***
