var app = angular.module('tpgFilterApp', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

/* region Custom filter */

app.filter('displayBadDomainsByProp', function() {

    /* region Process for UserAgent's strings */

    /**
     * @name declaredBrowsersList
     * @type Array
     */
    var declaredBrowsersList = [
        'opera',
        'chrome',
        'chromium',
        'safari',
        'firefox',
        'msie',
        'trident'
    ];

    /**
     *
     * @param userAgent
     * @returns {string}
     */
    function browserDetect(userAgent) {
        var temporary,
            regExpForSplit = new RegExp(
                "(" + declaredBrowsersList.join('|') + "(?=\\/))\\/?\\s*(\\d+)", 'i'
            ),
            M = userAgent.match(regExpForSplit) || [];

        if (/trident/i.test(M[1])) {
            temporary = /\brv[ :]+(\d+)/g.exec(userAgent) || [];
            return 'IE ' + (temporary[1] || '');
        }

        if (M[1] === 'Chrome') {
            temporary = userAgent.match(/\b(OPR|Edge)\/(\d+)/);
            if (temporary != null) return temporary.slice(1).join(' ').replace('OPR', 'Opera');
        }

        M = M[2] ? [M[1], M[2]] : [userAgent, '-?'];

        if ((temporary = userAgent.match(/version\/(\d+)/i)) != null) {
            console.warn(M.splice(1, 1, temporary[1]));
            M.splice(1, 1, temporary[1]);
        }

        return M.join(' ');
    }

    /* endregion Process for UserAgent's strings */

    /* region of Cut the string methods */


    function convertParamsToBeauty(stringValue, length) {
        var endSymbols = '...';
        if (length === undefined) {
            length = 32;
        }

        /* if length well */
        if (stringValue.length < length) {
            return stringValue;
        }

        /* if length small then rule+3*/
        if (stringValue.length < (length + endSymbols.length)) {
            if (stringValue.substr(-endSymbols.length) === endSymbols) {
                return stringValue;
            }
        }

        return stringValue.substr(0, length) + endSymbols;
    }

    /* endregion of Cut the string methods */

    return function(input, prop, place) {
        switch (place) {
            case 'title':
                return input[prop];
            case 'cell':
            default:
                var out = input[prop];
        }

        switch (prop) {
            case 'bad_domain':
                return (out) ? 'Yes' : 'No';
            case 'ua':
                return browserDetect(out);
            case 'ref':
                if (!out || (input[prop] === '- Direct -')) {
                    out = input[prop] = out ? out : '- Direct -';
                }
                return out;
            case 'param1':
            case 'param2':
                if (!out || (input[prop] === '- No param -')) {
                    return input[prop] = '- No param -';
                }
                return convertParamsToBeauty(out, 20);
            default:
                return out;
        }


    };

    // conditional based on optional argument
    /*if (uppercase) {
     out = out.toUpperCase();
     }*/
});

/* endregion Custom filter */

app.controller('mainController', function ($scope, $http) {

    /* region services Counters declaration */

    /**
     * @description Counters object.
     *
     * Object contain:
     *      `total` - Quantity of all records what AngularJS get from API.
     *      `filtered` - Quantity what display after filters had been apply.
     *
     * @type {{total: number, filtered: number}}
     */
    $scope.counters = ({
        'total': 0,
        'filtered': 0,
        'checkProvidedParams': function (n) {
            if (Number.isInteger(n)) {
                return n;
            } else {
                throw new Error('Wrong type of provided params');
            }
        },
        'setTotal': function (n) {
            this.total = this.filtered = this.checkProvidedParams(n);
        },
        'handlerFiltersApplied': function (n) {
            this.filtered = this.checkProvidedParams(n);
        }
    });

    /* endregion services Counters declaration */

    /**
     * @description
     * List of properties at Object.
     *
     * @type Array
     */
    $scope.propertiesList = [
        'id',
        'ip',
        'ua',
        'param1',
        'param2',
        'ref',
        'error',
        'bad_domain'
    ];

    /**
     * @description Variables uses at ng-repeat for object "BadDomains"
     * @name prop
     * @type {string}
     */
    $scope.prop = '';

    /**
     * @description
     * Wrapper for putting and storage Data Collection from API.
     * @see $scope.badDomain
     * @type {Array}
     */
    $scope.badDomains   = [];

    /**
     * @description
     * Variables uses at ng-repeat for object "BadDomains"
     * @name badDomain
     * @type {{id: string, ua: string, ip: string, ref: string, param1: string,
     * param2: string, error: string, bad_domain: string}}
     */
    $scope.badDomain = ({});

    /**
     * @description
     * Wrapper for putting and storage Data Collection with from badDomains after filter applied.
     * @type {Array}
     */
    $scope.badDomainsFiltered = [];

    $scope.$watchCollection('badDomainsFiltered', function(arr) {
        $scope.counters.handlerFiltersApplied(arr.length);
    });

    /**
     * @see badDomain
     */
    $scope.searchModel = ({});
    /*
     // Does not use it, cause it calls wrong behaviour while filtering
     for (var prop in $scope.propertiesList) {
     if ($scope.propertiesList.hasOwnProperty(prop)) {
     $scope.searchModel[prop] = '';
     }
     }*/

    $scope.sortType     = 'id';
    $scope.sortReverse  = false;

    $scope.setSortBy = function (prop) {
        if ($scope.propertiesList.includes(prop)) {
            /* Change direction only if prop the same */
            if ($scope.sortType === prop) {
                $scope.sortReverse = !$scope.sortReverse;
            }

            $scope.sortType = prop;
        }
    };

    $scope.init = function () {
        $http.get('/api/clicks').success(function (data) {
            $scope.counters.setTotal(data.length);
            $scope.badDomains = data;
        });
    };

    $scope.init();
});
