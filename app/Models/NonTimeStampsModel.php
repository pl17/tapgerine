<?php

namespace Tapgerine\TSp\Models;

use Illuminate\Database\Eloquent\Model;

class NonTimeStampsModel extends Model
{
    public $timestamps = false;
}
