<?php

namespace Tapgerine\TSp\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Psy\Exception\ErrorException;
use Tapgerine\TSp\Helpers\StringGenerator;

/**
 * Class Click
 *
 * Class dependence on BadDomain.
 * @package Tapgerine\TSp\Models
 *
 * @property string $id
 * @property string $ip
 * @property string $ua
 * @property string $param1
 * @property string $param2
 * @property string $ref
 * @property integer $error
 * @property boolean|integer $bad_domain
 *
 * @see BadDomain
 */
class Click extends NonTimeStampsModel
{
    protected $table = 'click';
    
    public $incrementing = false;
    protected $keyType = 'string';
    
    protected $fillable = [
        'id',
        'ip',
        'ua',
        'param1',
        'param2',
        'ref'
    ];
    
    /**
     * List of params what checked when visitor is unique.
     *
     * @var array
     */
    protected $paramsCheckedOnUnique = [
        'ref',
        'ip',
        'ua',
        'param1'
    ];
    
    protected $isFresh = true;
    
    /**
     * @param Request $request
     * @return $this
     * @throws ErrorException
     */
    public function buildModel(Request $request)
    {
        /* check before all actions */
        if ($this->isJustCreatedAndEmpty()) {
            throw new ErrorException('Can\'t filling existing model');
        }
        
        $errorFlag = $badDomainFlag = false;
        
        $this->initBuild($request);
        
        if (!$this->isNonUniqueByParamsSelectModel()) {
            $this->continueBuild($request);
        }
        
        if ($badDomainFlag = $this->validateReferrer()) {
            $errorFlag = true;
        }
        
        if ($errorFlag) {
            $this->incrementError();
        }
        
        $this->save();
        
        return $this;
    }
    
    /**
     * Method return boolean whether click is unique by params.
     * @return boolean
     */
    public function isNonUniqueByParamsSelectModel()
    {
        $where = $whereNull = [];
        
        foreach ($this->paramsCheckedOnUnique as $field) {
            
            if (is_null($this->$field)) {
                array_push($whereNull, $field);
            } else {
                array_push($where, $this->getFieldsConditionRules($field, $this->$field));
            }
        }
        
        /** @var \Illuminate\Database\Query\Builder $qBuilder */
        $qBuilder = self::where($where);
        
        foreach ($whereNull as $field) {
            $qBuilder = $qBuilder->whereNull($field);
        }
        
        if ((boolval($qBuilder->count() === 0))) {
            return false;
        }
    
        $this->selectingRecordByOtherRecord($qBuilder->first());
        
        return true;
    }
    
    /**
     * @param Request $request
     */
    protected function initBuild(Request $request)
    {
        $this->ua = $request->header('User-Agent');
        $this->ip = $request->ip();
        $this->ref = $request->header('Referer');
        $this->param1 = $request->input('param1');
    }
    
    /**
     * @param Request $request
     */
    protected function continueBuild(Request $request)
    {
        $this->param2 = $request->input('param2');
        $this->id = StringGenerator::uniqueRandomString('bad_domains', 'id');
    }
    
    /**
     * @param array $attributes
     * @return Model||$this
     */
    public function fill(array $attributes)
    {
        if (count($attributes) !== 0) {
            $this->setLikeNonFresh();
        }
        
        return parent::fill($attributes);
    }
    
    /**
     * @param string $key
     * @param mixed $value
     */
    public function __set($key, $value)
    {
        $this->setLikeNonFresh();
        $this->setAttribute($key, $value);
    }
    
    /**
     *
     */
    public function setLikeNonFresh()
    {
        $this->isFresh = false;
    }
    
    /**
     * @return boolean
     */
    public function isJustCreatedAndEmpty()
    {
        return boolval($this->exists || !$this->isFresh);
    }
    
    /**
     * @return boolean
     */
    public function validateReferrer()
    {
        if ($this->bad_domain) {
            return true;
        }
        
        if (!$this->ref) {
            return false;
        }
        
        if (BadDomain::checkIsDomainBad($this->ref)) {
            $this->bad_domain = true;
        }
        
        return boolval($this->bad_domain);
    }
    
    /**
     *
     */
    protected function incrementError()
    {
        $this->error++;
    }
    
    /**
     * @param $fieldName
     * @param $value
     * @return array
     */
    protected function getFieldsConditionRules($fieldName, $value)
    {
        switch ($fieldName) {
            case 'ref':
                $value = '%' . $value . '%';
                // no break;
            default:
                $operand = 'like';
        }
        
        return [$fieldName, $operand, $value];
    }
    
    /**
     * @param $source
     */
    protected function selectingRecordByOtherRecord($source)
    {
        $this->forceFill($source->attributes);
        $this->original = $this->attributes;
        $this->exists = true;
    }
    
    /**
     * @return bool
     */
    public function isBadDomain()
    {
        return boolval($this->bad_domain);
    }
    
    /**
     * @return bool
     */
    public function isError()
    {
        return boolval($this->error);
    }
    
    /**
     * Method return string format of ID.
     * @return string
     */
    public function getID()
    {
        return $this->attributes['id'];
    }
}
