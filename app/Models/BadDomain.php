<?php

namespace Tapgerine\TSp\Models;

/**
 * Class BadDomain
 * @package Tapgerine\TSp\Models
 * @extends Tapgerine\TSp\Models\NonTimeStampsModel
 *
 * @property int $id
 * @property string $name
 */
class BadDomain extends NonTimeStampsModel
{
    /* override default value */
    protected $perPage = 30;
    protected $fillable = ['name'];
    
    protected static $editingMethods = [
        'PUT', 'PATCH'
    ];
    
    /**
     * @param string $method
     * @return array
     */
    public function rules(string $method = 'POST')
    {
        $patchConditionString = in_array($method, self::$editingMethods) ? (',' . $this->id) : '';
        
        return [
            'name' => 'required|url|unique:bad_domains,name' . $patchConditionString
        ];
    }
    
    /**
     * @return mixed
     */
    public function getPaginatedList()
    {
        return self::paginate();
    }
    
    /**
     * @param string $link
     * @return bool
     */
    public static function checkIsDomainBad(string $link)
    {
        if (!$link) {
            return false;
        }
        
        $link = self::constructCleanUrlHelper($link);
        
        /* set like valid users without referral link */
        if (!$link) {
            return false;
        }
        
        return boolval(self::where('name', 'like', '%' . $link . '%')->count());
    }
    
    protected static function constructCleanUrlHelper(string $link)
    {
        $objLink = parse_url($link);
        return (isset($objLink['host'])) ? $objLink['host'] : '';
    }
}
