<?php
/**
 * Project:     tapgerine
 * File:        StringGenerator.php
 * Author:      planet17
 * DateTime:    M05.D29.2017 1:24 AM
 */

namespace Tapgerine\TSp\Helpers;

use Illuminate\Support\Facades\DB;

class StringGenerator
{
    /**
     * @param $table
     * @param $col
     * @param integer $chars
     * @return boolean|string
     */
    public static function uniqueRandomString($table, $col, $chars = 32)
    {
        $unique = false;
        $checkedArray = [];
        
        do {
            $time = (string)time();
            $random = str_random($chars - strlen($time));
            
            $random = substr(($time . $random), 0, $chars);
    
            if (in_array($random, $checkedArray)) {
                continue;
            }
            
            $count = DB::table($table)->where($col, '=', $random)->count();
            
            
            $checkedArray[] = $random;
            
            if ($count == 0) {
                $unique = true;
            }
        } while (!$unique);
        
        return $random;
    }
}