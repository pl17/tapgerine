<?php
/**
 * Project:     tapgerine
 * File:        laravel-link-widget.php
 * Author:      planet17
 * DateTime:    M06.D01.2017 6:09 PM
 *
 * It is required package LaravelCollective/Html
 */
if (!function_exists('tpgNavMenuHelperWithShortName')) {
    /**
     * Generate a HTML link to a named route.
     *
     *
     * @param $routeName
     * @param null $titles
     * @param array $parameters
     * @param string $classes
     * @return string|\Illuminate\Support\HtmlString|string
     */
    function tpgNavMenuHelperWithShortName(
        $routeName,
        $titles = null,
        $parameters = [],
        $classes = 'btn btn-small btn-success'
    )
    {
        if (is_array($titles)) {
            list($tFRoute, $tFTitle) = $titles;
        } elseif (is_null($titles)) {
            $tFTitle = $tFRoute = null;
        } elseif(is_string($titles)) {
            $tFTitle = $titles;
            $tFRoute = strtoupper(substr($titles, 0, 1));
        } else {
            throw new ErrorException('Wrong type of $titles.');
        }
        
        $attributes = [
            'class' => $classes
        ];
        
        if (!is_null($tFTitle)) {
            $attributes = array_merge($attributes, [
                'title' => $tFTitle
            ]);
        }
        
        return app('html')->linkRoute(
            $routeName,
            $tFRoute,
            $parameters,
            $attributes
        );
    }
}
