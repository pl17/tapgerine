<?php

namespace Tapgerine\TSp\Http\Controllers;

use Illuminate\Http\Request;
use Tapgerine\TSp\Models\Click;

class ClicksResourceController extends Controller
{
    
    public function apiFullList()
    {
        $listCollection = Click::all();
        return $listCollection->toJson();
    }
}
