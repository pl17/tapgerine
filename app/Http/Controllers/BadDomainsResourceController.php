<?php

namespace Tapgerine\TSp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Tapgerine\TSp\Models\BadDomain;

class BadDomainsResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param BadDomain $model
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BadDomain $model)
    {
        $dataProvider = $model->getPaginatedList();
        return view('bad-domains.list', [
            'data' => $dataProvider
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param BadDomain $model
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, BadDomain $model)
    {
        /** @var \Illuminate\Contracts\Validation\Validator $validator */
        $validator = Validator::make(
            Input::all(),
            $model->rules($request->method())
        );
        
        if ($validator->fails()) {
            
            return Redirect::route('bad-domains.create')
                ->withErrors($validator)
                ->withInput(Input::all());
            
        } else {
            $model->fill($request->all());
            $msg = $model->save() ? 'Successfully created BadDomain!' : 'Some shit happens while we trying create a new BadDomain';
            return $this->redirectWithFlash($msg);
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Response::create(
            (
                'Show action is not implemented.<br>' .
                link_to_route('bad-domains.index', 'Back to list') .
                '<br>Or you can go to ' .
                link_to_route('bad-domains.edit', 'edit that', $id)
            ),
            Response::HTTP_NOT_IMPLEMENTED
        );
    }

    /**
     * Show the form for editing the specified resource.
     * @param BadDomain $model
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(BadDomain $model, int $id = null)
    {
        if (!$id) {
            /* from create */
            $template = 'bad-domains.create';
            $dataProvider = $model->newInstance();
            
        } else {
            /* direct to edit */
            $template = 'bad-domains.edit';
            $dataProvider = $model::find($id);
            if (!$dataProvider) {
                return $this->nonExist($id);
            }
        }
    
        return view($template, [
            'dataProvider' => $dataProvider
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @param BadDomain $model
     * @return \Illuminate\Http\Response
     */
    public function create(BadDomain $model)
    {
        return $this->edit($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BadDomain  $model
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BadDomain $model, Request $request, $id)
    {
        /* Check if exist */
        $dataProvider = BadDomain::find($id);
    
        if (!$dataProvider) {
            return $this->nonExist($id);
        }
        
        /** @var \Illuminate\Contracts\Validation\Validator $validator */
        $validator = Validator::make(
            Input::all(),
            $dataProvider->rules($request->method())
        );
    
        if ($validator->fails()) {
            return Redirect::route('bad-domains.edit', $id)
                ->withErrors($validator)
                ->withInput(Input::all());
        
        }
        
        $dataProvider->name = Input::get('name');
        $msg = $dataProvider->save() ? 'Successfully updated BadDomain!' : 'Some shit happens while we trying update BadDomain';
        return $this->redirectWithFlash($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  BadDomain $model
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BadDomain $model, int $id)
    {
        $msg = "Removing of element {{$id}} had been ";
        $msg .= ($model->destroy($id) ? 'successfully!' : 'failed');
        
        return $this->redirectWithFlash($msg);
    }
    
    public function nonExist(int $id)
    {
        return $this->redirectWithFlash( "Element {{$id}} not found!");
    }
    
    private function redirectWithFlash($msg)
    {
        Session::flash('message', $msg);
        return Redirect::route('bad-domains.index');
    }
}
