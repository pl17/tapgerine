<?php

namespace Tapgerine\TSp\Http\Controllers;

use Tapgerine\TSp\Models\Click;
/* default */
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\View\View;

/**
 * Class SerfLinksLoggerController
 * @package Tapgerine\TSp\Http\Controllers
 */
class SerfLinksLoggerController extends Controller
{
    /**
     * Method return view for Application HomePage.
     * @return ViewFactory|View
     */
    public function index()
    {
        return view('welcome');
    }
    
    /**
     * @param Request $request
     * @param Click $model
     * @return mixed
     */
    public function trackingLinkAnalyzer(Request $request, Click $model)
    {
        /* var \Illuminate\Contracts\Validation\Validator $validator
         * $validator = Validator::make( Input::all(), $model->rules($request->method()) );*/
        
        $model->buildModel($request);
        
        $params = ['id' => $model->getId()];
        $route = 'serf-link-logger.r-';
        $route .= $model->isError() ? 'fail' : 'success';
        
        return Redirect::route($route, $params);
    }
    
    /**
     * @param $id
     * @param Click $model
     * @param Request $request
     * @return ViewFactory|View
     */
    public function trackingResult($id, Click $model, Request $request)
    {
        /** @var Click $dataProvider */
        if (!($dataProvider = $model->find($id))) {
            return Redirect::route('welcome');
        }
        
        if (!$this->validateUnique($dataProvider, $request)) {
            return Redirect::route('welcome');
        }
        
        return view('serf-links-logger.tracking-results', [
            'data' => $dataProvider,
            'redirect' => $dataProvider->isBadDomain(),
            'status' => !$dataProvider->isError()
        ]);
    }
    
    /**
     * @param Click $model
     * @param Request $request
     * @return boolean
     */
    protected function validateUnique(Click $model, Request $request): bool
    {
        $uniqueFlag = boolval(stripos($request->path(), 'error') === false);
        return boolval($uniqueFlag === !$model->isError());
    }
    
    /**
     * @return ViewFactory|View
     */
    public function exampleLinksDemo()
    {
        $links = [];
        
        for ($i = 10; $i--; ) {
            $links[$i] = [
                'param1' => str_random(rand(5, 128)),
                'param2' => str_random(rand(5, 128)),
            ];
        }
        
        $links = array_reverse($links);
        
        return view('serf-links-logger.list-generated-links', [
            'data' => $links
        ]);
    }
}
