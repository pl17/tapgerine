<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClicksTable extends Migration
{
    private $tableName = 'click';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->string('id', 32)->index()->unique();
            $table->string('ua')->nullable();
            $table->ipAddress('ip')->nullable();
            $table->string('ref')->nullable();
            $table->string('param1', 128)->nullable();
            $table->string('param2', 128)->nullable();
            $table->unsignedSmallInteger('error')
                ->default(0)
                ->unsigned();
            $table->tinyInteger('bad_domain')
                ->default(0);
            
            
            $table->unique([
                'ua',
                'ip',
                'ref',
                'param1'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
    
}
