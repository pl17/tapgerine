<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SerfLinksLoggerController@index')->name('welcome');
/* Return public data for AngularJS uses at `welcome` */
Route::get('/api/clicks', 'ClicksResourceController@apiFullList')->name('api.home-page-data');
/* Page of click processing */
Route::get('/click', 'SerfLinksLoggerController@trackingLinkAnalyzer')->name('serf-link-logger.analyzer');
Route::get('/success/{id}', 'SerfLinksLoggerController@trackingResult')->name('serf-link-logger.r-success');
Route::get('/error/{id}', 'SerfLinksLoggerController@trackingResult')->name('serf-link-logger.r-fail');
Route::get('/link-generator', 'SerfLinksLoggerController@exampleLinksDemo')->name('serf-link-logger.links-generator');

/* Fully resource map for Domains */
Route::resource('bad-domains', 'BadDomainsResourceController');
