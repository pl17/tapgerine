<?php
/**
 * Project:     tapgerine
 * File:        list.blade.php
 * Author:      planet17
 * DateTime:    M05.D25.2017 11:38 PM
 */
?>
@extends('layouts.default')
@section('body')
    <div class="flex-center position-ref cut-height">
        @include('common._cn-links')
    </div>

    @if(count($data))
        <div class="flex-center position-ref">
            <h2>List (total {{$data->total()}} BadDomains)</h2>
            <h3>Page {{$data->currentPage()}} from {{$data->lastPage()}}</h3>
        </div>

        @if(Session::has('message'))
            <div class="alert alert-info">
                {!! Session::get('message') !!}
            </div>
        @endif

        <div class="f-right">
            <a class="btn btn-success" href="{{route('bad-domains.create')}}">Create</a>
        </div>

        <div class="f-center position-ref data">
            <ul class="flex-list">
                @foreach($data as $domain)
                    <li>
                        <div class="id">
                            {{$domain->id}}
                        </div>

                        <div class="name">
                            {{$domain->name}}
                        </div>

                        <div class="edit">
                            {!! link_to_route(
                                'bad-domains.edit',
                                'Edit',
                                $domain->id,
                                ['class' =>  'btn btn-small btn-warning']
                            ) !!}
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="flex f-center position-ref pagination">
            {{$data->links()}}
        </div>
    @else
        <div class="flex-center position-ref">
            <h2>Bad Domains does not found.</h2>
        </div>
    @endif
@stop
