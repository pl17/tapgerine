<div class="form-group">
    {!! Form::label('Type the full link...') !!}
    {!! Form::text('name', $dataProvider->name, [
        'class'=>'form-control',
        'placeholder' => 'http://example.com',
        'autofocus' => 1
    ]) !!}
</div>
<div class="form-group">
    {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
</div>

<a class="btn btn-default" href="{{route('bad-domains.index')}}">Go back</a>
