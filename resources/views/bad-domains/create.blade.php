<?php
/**
 * Project:     tapgerine
 * File:        list.blade.php
 * Author:      planet17
 * DateTime:    M05.D25.2017 11:38 PM
 */
?>
@extends('layouts.default')
@section('body')

    <h1>Create</h1>

    <div class="errors">
        {{ Html::ul($errors->all()) }}
    </div>

    {!! Form::open(['route'=> 'bad-domains.store']) !!}
        @include('bad-domains._form')
    {!! Form::close() !!}
@stop
