<?php
/**
 * Project:     tapgerine
 * File:        list.blade.php
 * Author:      planet17
 * DateTime:    M05.D25.2017 11:38 PM
 */
?>
@extends('layouts.default')
@section('body')

    <h1>Edit</h1>

    <div class="errors">
        {{ Html::ul($errors->all()) }}
    </div>

    {!! Form::open([
        'route'=> ['bad-domains.update', $dataProvider->id],
        'method' => 'put'
    ]) !!}
        @include('bad-domains._form')
    {!! Form::close() !!}

    {!! Form::open([
        'route' => ['bad-domains.destroy', $dataProvider->id],
        'method' => 'delete'
    ]) !!}
    {!! Form::submit('Remove', ['class' => 'btn btn-danger glyphicon glyphicon-trash']) !!}
    {!! Form::close() !!}
@stop
