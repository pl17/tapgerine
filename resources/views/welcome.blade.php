@extends('layouts.default')
@section('body')
    {{--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.11/angular.min.js"></script>--}}
    <script src="{{asset('/js/angular-js/angular.min.js')}}"></script>
    <script src="{{asset('/js/angular-js/app.js')}}"></script>
    <div class="flex-center position-ref cut-height">
        @include('common._cn-links')
    </div>
    @include('serf-links-logger.welcome')
@stop
