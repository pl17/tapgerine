<?php
/**
 * Project:     tapgerine
 * File:        _cn_links.blade.php
 * Author:      planet17
 * DateTime:    M05.D25.2017 10:40 PM
 */
?>
<div class="content">
    <div class="title m-b-md">
        {{ config('app.name') }}
    </div>

    <div class="links">
        {!! tpgNavMenuHelperWithShortName('welcome', 'Home') !!}
        {!! tpgNavMenuHelperWithShortName(
            'serf-link-logger.analyzer',
            ['t', 'Permanent test\'s links'],
            'param1=178&param2=helloTapgerine'
        ) !!}
        {!! tpgNavMenuHelperWithShortName('bad-domains.index', 'Bad Domains') !!}
    </div>
</div>
