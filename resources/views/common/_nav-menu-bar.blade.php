<?php
/**
 * Project:     tapgerine
 * File:        _nav-menu-bar.blade.php
 * Author:      planet17
 * DateTime:    M05.D27.2017 12:39 PM
 */
?>
<div class="top-right links" style="z-index: 2">
    {!! link_to_route('welcome', 'Home', [], ['class' =>  'btn btn-small btn-success'] ) !!}
@if (Route::has('serf-link-logger.links-generator'))
        {!! link_to_route('serf-link-logger.links-generator', 'Generate links for click', [], [
            'class' => 'btn btn-small btn-warning'
        ]) !!}
    @endif
    {!! link_to_route('bad-domains.index', 'Bad domains', [], ['class' =>  'btn btn-small btn-success']) !!}
</div>
