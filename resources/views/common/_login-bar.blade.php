<?php
/**
 * Project:     tapgerine
 * File:        _login-bar.blade.php
 * Author:      planet17
 * DateTime:    M05.D25.2017 10:43 PM
 */
?>
@if (Route::has('login'))
    <div class="top-right links">
        @if (Auth::check())
            {!! link_to_route('welcome', 'Home') !!}
        @else
            {!! link_to_route('welcome', 'Login') !!}
            {!! link_to_route('welcome', 'Register') !!}
        @endif
    </div>
@endif
