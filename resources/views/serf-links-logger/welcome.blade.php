<div class="angular-js-container" ng-app="tpgFilterApp" ng-controller="mainController">

    @if(config('app.debug') && 0)
        <div class="debug-panel alert alert-info">
            <p>Sort Type: <% sortType %></p>
            <p>Sort Reverse: <% sortReverse %></p>
            <p>Direction:
                <span ng-if="sortReverse">DESC</span>
                <span ng-if="!sortReverse">ASC</span>
            </p>
        </div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <table id="table-ng" class="table table-striped">
                    <caption>
                        <hr>
                        Total <b><% counters.total %></b> records
                        (matches: <b><% counters.filtered %></b>)
                    </caption>
                    {{--search-inputs--}}
                    <tr>
                        <td>Filter</td>
                        <td ng-repeat="prop in propertiesList"
                            ng-class="[{'sorted-col': sortType == prop}]">

                            {{--Write custom FieldTypeByProp--}}
                            <input type="search"
                                   placeholder="Type <% prop | uppercase %>..."
                                   ng-model="searchModel[prop]">
                        </td>
                    </tr>
                    {{--sorts-buttons--}}
                    <tr class="sort-buttons">
                        <td>SORT</td>
                        <td ng-repeat="prop in propertiesList"
                            ng-class="[{'sorted-col': sortType == prop}]">
                            <a href="#table-ng"
                               ng-click="setSortBy(prop)"
                               class="btn btn-sm"
                               ng-class="[{'btn-warning': sortType != prop}, {'btn-info': sortType == prop}]">
                                <b><% prop | uppercase %></b>
                                <span class="glyphicon glyphicon-menu-up"
                                      ng-show="sortType == '<% prop %>' && !sortReverse"
                                      title="ASC sort <% prop | uppercase %>">
                                </span>
                                <span class="glyphicon glyphicon-menu-down"
                                      ng-show="sortType == '<% prop %>' && sortReverse"
                                      title="DESC sort by <% prop | uppercase %>">
                                </span>
                            </a>
                        </td>
                    </tr>
                    {{--data-rows--}}
                    <tr ng-repeat='(p, badDomain) in badDomains | filter : searchModel | orderBy : sortType : sortReverse as badDomainsFiltered'>
                        <td><% (p+1) %></td>
                        <td ng-repeat="prop in propertiesList"
                            ng-class="[{'sorted-col': sortType == prop}]">
                            {{-- Write custom Pipe by Prop --}}
                            <b title="<% badDomain | displayBadDomainsByProp : prop: 'title' %>">
                                <% badDomain | displayBadDomainsByProp : prop: 'cell' %>
                            </b>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>