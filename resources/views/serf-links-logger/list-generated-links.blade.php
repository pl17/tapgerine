@extends('layouts.default')
@section('body')
    @foreach($data as $k => $link)
        {!! link_to_route('serf-link-logger.analyzer',
        "Links $k",
        [ 'param1' => $link['param1'], 'param2' => $link['param2'] ],
        ['class' =>  'btn btn-small btn-success']) !!}
        <hr>
    @endforeach
@stop
