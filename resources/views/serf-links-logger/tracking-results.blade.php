@extends('layouts.default')
@section('body')
    @if($redirect)
        <meta http-equiv="refresh" content="5; url=http://google.com/"/>
    @endif

    <h1>
        Your click is
        @if(!$status) not @endif()
        unique!
    </h1>

    <div class="content">
        <table class="table table-striped" style="font-weight: 600;">
            <thead style="font-weight: 700; font-size: 36px;"> Your data:</thead>
            <tbody>
            <tr>
                <td>IP:</td>
                <td>{{$data->ip}}</td>
            </tr>
            <tr>
                <td>Browser:</td>
                <td>{{$data->ua}}</td>
            </tr>
            <tr>
                <td>Come from:</td>
                <td>
                    @if($data->ip)
                        {{$data->ip}}
                    @else
                        Direct
                    @endif
                </td>
            </tr>
            <tr>
                <td>Parameter 1:</td>
                <td>{{$data->param1}}</td>
            </tr>
            <tr>
                <td>Parameter 2:</td>
                <td>{{$data->param2}}</td>
            </tr>
            <tr>
                <td>Count of error:</td>
                <td>{{$data->error}}</td>
            </tr>
            <tr>
                <td>Did your come from bad domain:</td>
                <td>

                    @if($data->bad_domain)
                        Yes
                    @else
                        No
                    @endif
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    {!! link_to_route(
        'welcome',
        'Go home',
        [],
        ['class' =>  'btn btn-default']
    ) !!}
@stop

